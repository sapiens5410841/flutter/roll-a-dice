import 'dart:math';

import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
      title: "RollADice",
      home: Home()
  ));
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _numeroRolado = 20;

  void RolarDados(){
    setState(() {
      _numeroRolado = Random().nextInt(20) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Rolada')),
      body: Container(child:
      Column(children: [
        Padding(
          padding: const EdgeInsets.all(80.0),
          child: Image.asset("images/d20.png"),
        ),
        Text("Você Rolou:",style: TextStyle(color: Colors.black,
            fontSize: 30
        ),),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 40, 0, 40),
          child: Text("${_numeroRolado}",style: TextStyle(fontWeight: FontWeight.w900,
              fontSize: 60
          ),),
        ),
        ElevatedButton(onPressed: RolarDados, child:
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text("Rolar",style: TextStyle(fontSize: 20),),
        ))
      ],)
      ),
    );
  }
}
